from django.shortcuts import render, redirect
from .models import Img
from .forms import ImgForm
from django.http.response import HttpResponse, FileResponse
from samplesite.settings import BASE_DIR
from datetime import datetime
import os
from django.contrib.auth import authenticate, login, logout
from django.contrib import sessions


def index(request):
    if request.method == 'GET':
        return HttpResponse('<h1>Hello</h1>')


def add(request):
    if request.method == 'POST':
        form = ImgForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('testapp:index')
    else:
        form = ImgForm()
        return render(request, 'testapp/add.html', context={'form': form})


def show(request):
    if request.method == 'GET':
        images = Img.objects.all()
        return render(request, 'testapp/SHOW.html', context={'images': images})


def delete(request, pk):
    img = Img.objects.get(pk=pk)
    img.img.delete()
    img.delete()
    return redirect('testapp:index')


FILES_ROOT = os.path.join(BASE_DIR, 'files')


# сохранение файла низкоуровневыми средствами, без использования модели и без записи в неё
def lowleveladd(request):
    if request.method == 'POST':
        form = ImgForm(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES['img']
            fn = f'{datetime.now().timestamp()}{os.path.splitext(uploaded_file.name)[1]}'
            fn = os.path.join(FILES_ROOT, fn)
            with open(fn, 'wb+') as destination:
                for chunk in uploaded_file.chunks():
                    destination.write(chunk)
            return redirect('testapp:index')
    else:
        form = ImgForm()
        return render(request, 'testapp/lowleveladd.html', context={'form': form})


def lowlevelindex(request):
    if request.method == 'GET':
        imgs = []
        for entry in os.scandir(FILES_ROOT):
            imgs.append(os.path.basename(entry))
            return render(request, 'testapp/lowlevelindex.html', context={'imgs': imgs})


# получение картинки для шаблона lowlevelindex.html
def get_image(request, filename):
    fn = os.path.join(FILES_ROOT, filename)
    return FileResponse(open(fn, 'rb'), content_type='application/octet-stream')


def my_login(request):  # Пример логина на более низком уровне
    username = request.POST('username')
    password = request.POST('password')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Вход выполнен
    else:
        # Вход не выполнен
        return HttpResponse('Try again')


def my_logout(request):
    logout(request)
    return redirect('testapp:index')


def test_cookie(request):
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
            return HttpResponse('<h1>WORKED!!!</h1>')
        else:
            return HttpResponse('<h1>NOT WORKED!!!</h1>')
    request.session.set_test_cookie()
    return render(request, 'testapp/test_cookie.html')
