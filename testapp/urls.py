from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'testapp'
urlpatterns = [
    path('index/', index, name='index'),
    path('add/', add, name='add'),
    path('show/', show, name='show'),
    path('delete/<int:pk>', delete, name='delete'),
    path('low/', lowleveladd, name='low'),
    path('lowindex/', lowlevelindex, name='lowindex'),
    path('getimage/<path:filename>', get_image, name='getimage'),
    path('cookie_test/', test_cookie, name='test_cookie'),

]
