from django.db import models


class Img(models.Model):
    img = models.ImageField(verbose_name='Изображение', upload_to='images/%Y/%m/%d')
    desc = models.TextField()

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
