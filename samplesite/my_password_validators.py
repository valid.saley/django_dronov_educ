from django.core.exceptions import ValidationError


class NoForbiddenCharsValidator:  # Пример своего валидатора паролей
    def __init__(self, forbidden_chars=(' ',)):
        self.forbidden_chars = forbidden_chars

    def validate(self, password, user=None):
        chars = ','.join(self.forbidden_chars)
        for fc in self.forbidden_chars:
            if fc in password:
                raise ValidationError(f'Пароль не должен содержать недопустимые символы {chars}',
                                      code='forbidden_chars_present')

    def get_help_text(self):
        chars = ','.join(self.forbidden_chars)
        return f'Пароль не должен содержать недопустимые символы {chars}'
