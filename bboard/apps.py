from django.apps import AppConfig


class BboardConfig(AppConfig):
    name = 'bboard'

    def ready(self):
        import bboard.signals
