from .models import Rubric
from .views import index
from django.http.response import HttpResponse


class RubricMiddleware:
    def __init__(self, func):
        self.func = func

    def __call__(self, request):
        return self.func(request)

    # def process_template_response(self, request,
    #                               response):  # изменение содержимого контекста с помощью этого метода сработает только в случае если ответ контроллера будет в виде TemplateResponse
    #     response.context_data['rubrics'] = Rubric.objects.all()
    #     return response
