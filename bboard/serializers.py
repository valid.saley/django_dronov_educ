from .models import Rubric
from rest_framework import serializers


class RubricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rubric
        fields = ('id', 'name', 'order')
