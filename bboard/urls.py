from django.urls import path

from .views import search, BbRedirectView, index, BbByRubricView2, BbDetailView, BbAddView, BbEditView, BbDeleteView, \
    BbMonthArchiveView, RubricFormSet, api_rubrics, api_rubric_detail
from django.views.decorators.cache import cache_page

urlpatterns = [
    path('add/', BbAddView.as_view(), name='add'),
    path('', index, name='index'),
    path('<int:rubric_id>', BbByRubricView2.as_view(), name='by_rubric'),
    path('detail/<int:pk>', cache_page(60*5)(BbDetailView.as_view()), name='detail'),
    path('edit/<int:pk>', BbEditView.as_view(), name='edit'),
    path('delete/<int:pk>', BbDeleteView.as_view(), name='delete'),
    path('<int:year>/<int:month>', BbMonthArchiveView.as_view(), name='month'),
    path('detail/<int:year>/<int:month>/<int:day>/<int:pk>', BbRedirectView.as_view(), name='redirect'),
    path('formset/', RubricFormSet.as_view(), name='formset'),
    path('search/', search, name='search'),
    path('api/rubrics/<int:pk>', api_rubric_detail),
    path('api/rubrics/', api_rubrics),
]
