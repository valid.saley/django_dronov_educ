from .models import Rubric


def rubrics(request):  # Просто добавляем в каждый шаблон поле rubrics
    return {'rubrics': Rubric.objects.all()}
