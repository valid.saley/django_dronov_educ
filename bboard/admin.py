from django.contrib import admin
from django.db import models
from django.db.models import F
from django import forms

from .models import Bb, Rubric, Machine


class PriceListFilter(admin.SimpleListFilter):
    title = 'Категория цен'
    parameter_name = 'price'

    def lookups(self, request, model_admin):
        return (
            ('low', 'Низкая цена'),
            ('medium', 'Средняя цена'),
            ('high', 'Высокая цена'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'low':
            return queryset.filter(price__lt=500)
        if self.value() == 'medium':
            return queryset.filter(price__gte=500, price__lte=5000)
        if self.value() == 'high':
            return queryset.filter(price__gt=5000)


class BbAdmin(admin.ModelAdmin):  # класс редактор
    # list_display = ('title', 'content', 'price', 'published', 'rubric')
    list_display = ('title_rubric', 'content', 'price', 'published')
    list_display_links = ['title_rubric', 'price']
    list_editable = ['content']  # если указать кортеж с одним значением, начинает ругаться, со списком всё нормально
    search_fields = ('title', 'content')
    ordering = ['price']
    sortable_by = ['title_rubric']
    list_filter = (PriceListFilter,)
    # list_filter = ['rubric']
    date_hierarchy = 'published'
    list_per_page = 5
    actions_on_bottom = True
    fieldsets = (
        (None, {  # None это базовый набор, без него ругается
            'fields': (('title', 'rubric'), 'content'),
            'classes': ('wide',)
        }),
        ('Доп. Сведения', {
            'fields': ('price',),
            'description': 'Параметры необязательные для указания'
        })
    )
    # radio_fields = {'rubric': admin.HORIZONTAL}  # не работает пока есть autocomplete_fields
    autocomplete_fields = ('rubric',)

    # formfield_overrides = {
    #     models.ForeignKey: {'widget': forms.widgets.Select(attrs={'size': 8})},
    # }

    # fields = (('title', 'price'), 'content')  # кортеж в кортеже, это вывод в одну строку
    actions = ['discount']

    def get_fields(self, request, obj=None):
        f = ['title', 'price', 'content']
        if not obj:
            f.append('rubric')
        return f

    def title_rubric(self, rec):  # Функциональное поле, которое обычно объявляется в моделях, глава 4.7
        return f'{rec.title}({rec.rubric.name})'

    title_rubric.short_description = 'Название и рубрика'  # Красивое название функционального поля

    # Функионального поля нету на уровне СУБД, поэтому для сортировки по функчиональному полю, мы это поле связали с существующими полями в модели
    title_rubric.admin_order_field = 'title'
    title_rubric.admin_order_field = 'rubric__name'

    def get_list_display(self, request):
        ld = ['title']  # Если пользователь не суперюзер, то он видит только тайтл
        if request.user.is_superuser:
            ld = ['title_rubric', 'content', 'price', 'published']
        return ld

    def discount(self, request, queryset):  # действие по уменьшению цены выбранных записей вдвое
        f = F('price')
        for rec in queryset:
            rec.price = f / 2
            rec.save()
        self.message_user(request, 'Цены изменены!')
    discount.short_description = 'Уменьшить цену вдвое'


class BbInline(admin.TabularInline):  # Таким образом создается встроееный редактор, который в последствии связывается с классом основного редактора
    # Этот редактор создает на странице редактирования связанной первичной модели, формы редакирования связанных вторихных моделей
    model = Bb  # Тоесть тут указывается вторичная модель
    show_change_link = True
    classes = ['collapse']  # вывод в виде свёрнутого списка


class RubricAdmin(admin.ModelAdmin):
    inlines = [BbInline]
    search_fields = ('name',)


class MachineAdmin(admin.ModelAdmin):
    filter_vertical = ('spares',)


admin.site.register(Bb, BbAdmin)  # Регистрация моделей в админке
admin.site.register(Rubric, RubricAdmin)  # Регистрация моделей в админке
admin.site.register(Machine, MachineAdmin)  # Регистрация моделей в админке
# Register your models here.
