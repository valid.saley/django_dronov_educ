from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from .models import *
from django.template import loader
from django.template.response import TemplateResponse
from django.views.generic.edit import CreateView
from .forms import *
from django.urls import reverse_lazy
from django.http import FileResponse, JsonResponse
from django.core.paginator import Paginator
from django.views.generic.base import TemplateView, RedirectView, View
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.views.generic.dates import ArchiveIndexView, MonthArchiveView, DateDetailView
from django.forms import modelformset_factory, inlineformset_factory
from django.forms.formsets import ORDERING_FIELD_NAME
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db import transaction
from django.db.models import Q
from django.contrib.messages.views import SuccessMessageMixin
from django.views.decorators.cache import cache_page
from .serializers import RubricSerializer
from rest_framework.response import Response as rest_response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated


def search(request):
    if request.method == 'POST':
        sf = SearchForm(request.POST)
        if sf.is_valid():
            keyword = sf.cleaned_data.get('keyword')
            rubric_id = sf.cleaned_data.get('rubric').id
            q = Q(title__contains=keyword) | Q(content__contains=keyword)
            bbs = Bb.objects.filter(q & Q(rubric=rubric_id))
            return render(request, 'bboard/search_result.html', context={'bbs': bbs})
        return redirect('index')


# class BbCreateView(CreateView):  # CreateView сам все сохраняет после пост запроса и валидации данных
#     template_name = 'bboard/create.html'
#     form_class = BbForm  # форма сама передается в контекст с именем form
#     success_url = reverse_lazy('index')  # просто ререврс по имени маршрута, и если есть, то надо как всегда передать url параметры
#
#     def get_context_data(self, **kwargs):  # дополняем контекст списком всех рубрик
#         context = super().get_context_data(**kwargs)
#         context['rubrics'] = Rubric.objects.all()
#         return context

# def add_and_save(request):
#     if request.method == 'POST':
#         bbf = BbForm(request.POST)
#         if bbf.is_valid():
#             bbf.save()
#             return HttpResponseRedirect(reverse('by_rubric', kwargs={'rubric_id': bbf.cleaned_data['rubric'].id}))
#         else:
#             return render(request, 'bboard/create.html', context={'form': bbf})
#
#     else:
#         bbf = BbForm
#         context = {'form': bbf}
#         return render(request, 'bboard/create.html', context)
#

# class BbAddView(FormView):
#     template_name = 'bboard/create.html'
#     form_class = BbForm
#     initial = {
#         'price': 0.0}  # Атрибут хранящий словарь для занеснеия данных в только что созданную форму(именя ключей должны совпадать с именами полей формы)
#
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         context['rubrics'] = Rubric.objects.all()
#         return context
#
#     def form_valid(self,
#                    form):  # метод выполняет свое тело если форма прошла валидацию, изначально идёт редирект на get_success_url()
#         form.save()
#         return super().form_valid(form)
#
#     def get_form(self,
#                  form_class=None):  # чтобы ниже перенаправится на спиок объявлений конкретной рубрики, нам нужно будет получить ключ нужной рубрики, поэтому нам нужно получить объект формы
#         self.object = super().get_form(form_class)
#         return self.object
#
#     def get_success_url(self):
#         return reverse('by_rubric', kwargs={'rubric_id': self.object.cleaned_data['rubric'].pk})

# Сделаем добавление без использования высокоуровневых классов(хотя это уже было)
class BbAddView(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = ('bboard.add_bb', 'bboard.change_bb', 'bboard.delete_bb')

    def get(self, request):
        bbf = BbForm(initial={'kind': 'b'})
        return render(request, 'bboard/create.html', context={'form': bbf})

    def post(self, request):
        bbf = BbForm(request.POST)
        if bbf.is_valid():
            bb = bbf.save(commit=False)  # Получаем новую запись но не сохраняем ее
            bb.kind = 's'
            try:  # Транзакции
                bb.save()
                transaction.commit()
            except:
                transaction.rollback()
            bbf.save_m2m()  # вызывать метод save_m2m(many to many) нужно только если мы юзали commit=False(как выше) и последующим вызовом метода save() модели
            return redirect(reverse('by_rubric', kwargs={'rubric_id': bbf.cleaned_data['rubric'].pk}))
        else:
            return render(request, 'bboard/create.html', context={'form': bbf})


@cache_page(60 * 5)
def index(request):
    bbs = Bb.objects.all()
    # rubrics = Rubric.objects.all()  # Убрали поле rubrics отсюда, и из контекста (там где page, bbs и sf) после того, как создали свой обработчик контекста в bboard.context_processors
    paginator = Paginator(bbs, 2)
    if 'page' in request.GET:
        page_num = request.GET['page']
    else:
        page_num = 1
    page = paginator.get_page(page_num)
    template = loader.get_template('bboard/index.html')
    context = {'page': page, 'bbs': page.object_list, 'sf': SearchForm()}
    return HttpResponse(template.render(context=context, request=request))
    # return render(request, 'bboard/index.html', context={'bbs': bbs, 'rubrics': rubrics})


# class BbIndexView(TemplateView):  # Меняем index (главную страницу) на класс
#     template_name = 'bboard/index.html'
#
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         context['bbs'] = Bb.objects.all()
#         context['rubrics'] = Rubric.objects.all()
#         return context

# class BbIndexView(ArchiveIndexView):
#     model = Bb
#     date_field = 'published'
#     template_name = 'bboard/index.html'
#     context_object_name = 'bbs'  # Имя переменной для использования в текущем контексте, как я понял это ставится в соответствие модели model=Bb, остальной конекст зааддется через get_context_data
#     allow_empty = True
#     date_list_period = 'year'  # урезанные даты идут в переменную шаблона date_list
#
#     def get_context_data(self, *args, **kwargs):
#         context = super().get_context_data(*args, **kwargs)
#         context['rubrics'] = Rubric.objects.all()
#         return context


class BbDetailView(DetailView):
    model = Bb
    template_name = 'bboard/detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['rubrics'] = Rubric.objects.all()
        return context


# def by_rubric(request, rubric_id):
#     bbs = Bb.objects.filter(rubric=rubric_id)
#     rubrics = Rubric.objects.all()
#     current_rubric = Rubric.objects.get(id=rubric_id)
#     context = {'bbs': bbs,
#                'rubrics': rubrics,
#                'current_rubric': current_rubric}
#     return render(request, 'bboard/by_rubric.html', context=context)

class BbByRubricView(ListView):
    template_name = 'bboard/by_rubric.html'
    context_object_name = 'bbs'  # Получается context_object_name задает имя переменной контекста, в которой будет сохранен извлеченный набор записей(я так понял извлеченный из get_queryset)

    # Существует атрибут queryset хранящий исхрдный набор записей из которого будут извлекатся данные
    # Метод get_query_set возвращает queryset если он задан, или набор записей извлеченный из модели, что задана в атрибуте model
    def get_queryset(self):
        return Bb.objects.filter(rubric=self.kwargs[
            'rubric_id'])  # получается словарь kwargs в данном случае хранит URL-параметры, что указаныв маршруте

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['rubrics'] = Rubric.objects.all()
        context['current_rubric'] = Rubric.objects.get(id=self.kwargs['rubric_id'])
        return context


class BbEditView(SuccessMessageMixin, UpdateView):
    template_name = 'bboard/update.html'
    model = Bb
    form_class = BbForm
    success_url = '/bboard'
    success_message = 'Bb %(title)s has been successfully edited'  # Специальный синтаксис для обращения к опред. полю формы

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['rubrics'] = Rubric.objects.all()
        return context


class BbDeleteView(DeleteView):
    template_name = 'bboard/delete.html'
    model = Bb
    success_url = '/bboard'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['rubrics'] = Rubric.objects.all()
        return context


class BbMonthArchiveView(MonthArchiveView):  # из url-параметра извлекается имя month(для года year, для дня day...)
    model = Bb
    date_field = 'published'
    month_format = '%m'  # без указания формата не находит
    # context_object_name = 'bbs' Не обязательно т.к. набор записей относящиеся к заданному месяцу пойдут в шаблон с именем object_list


class BbRedirectView(RedirectView):
    url = '/bboard/detail/%(pk)d'  # редирект со строки 13 в urls.py на эту ссылку


# Ниже пример контроллера-класса смешанной функциональности

class BbByRubricView2(ListView, SingleObjectMixin):
    template_name = 'bboard/by_rubric.html'
    pk_url_kwarg = 'rubric_id'  # Имя URL-параметра, который содержит первичный ключ

    def get(self, request, *args,
            **kwargs):  # Непонятно откуда конкретно этот метод get, но тут мы просто находим одну нужную нам рубрику
        # ниже метод get_object ищет запись по ключу указанному в pk_url_kwarg
        self.object = self.get_object(
            queryset=Rubric.objects.all())  # как я понял на данном этапе переменная self.object это придуманная нами переменная
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_rubric'] = self.object
        context['rubrics'] = Rubric.objects.all()
        context['bbs'] = context[
            'object_list']  # В BbByRubricView что находится выше, мы определяли переменную контекста 'bbs' с помощью context_object_name, но тут такое не работет из-за каких-то особенностей наследования
        return context

    def get_queryset(self):
        return self.object.bb_set.all()  # Судя по результатам проверки возвращенный данным методом queryset как раз и идёт в переменную контекста object_list, который используется выше


class RubricFormSet(View):
    def get(self, request):
        if request.user.has_perms(('rubric.add_rubric', 'rubric.change_rubric', 'rubric.delete_rubric')):
            formset = RubricFormSett(initial=[{'name': 'new rub'}, {'name': 'one more new'}],
                                     queryset=Rubric.objects.all()[0:5])
            return render(request, 'bboard/formset.html', context={'form': formset})
        else:
            return redirect_to_login(reverse('formset'))

    def post(self, request):
        formset = RubricFormSett(request.POST, queryset=Rubric.objects.all()[
                                                        0:5])  # если указывали в get методе queryset, то тут тоже надо указать
        if formset.is_valid():
            formset.save(commit=False)  # В базу сохранение не идёт
            for form in formset:  # Проход по каждой форме формсета
                if form.cleaned_data:  # Если cleaned_data формы не пустая
                    rubric = form.save(commit=False)
                    rubric.order = form.cleaned_data[
                        ORDERING_FIELD_NAME]  # ORDERING_FIELD_NAME это поле которое появляется при modelformset_factory(can_order=True)
                    rubric.save()
            for rubric in formset.deleted_objects:  # просто показать что после сохранения в formset появляются три атрибута deleted_objects, new_objects, changed_objects
                rubric.delete()
            return redirect('index')


@api_view(['GET', 'POST'])
def api_rubrics(request):
    if request.method == 'GET':
        rubrics = Rubric.objects.all()
        serializer = RubricSerializer(rubrics, many=True)
        return rest_response(serializer.data)
    if request.method == 'POST':
        serializer = RubricSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return rest_response(serializer.data, status=status.HTTP_201_CREATED)
        return rest_response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
@permission_classes((IsAuthenticated,))
def api_rubric_detail(request, pk):
    rubric = Rubric.objects.get(pk=pk)
    if request.method == 'GET':
        serializer = RubricSerializer(rubric)
        return rest_response(serializer.data)
    elif request.method == 'PUT' or request.method == 'PATCH':
        serializer = RubricSerializer(rubric, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return rest_response(serializer.data)
        return rest_response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        rubric.delete()
        return rest_response(status=status.HTTP_204_NO_CONTENT)

# Примеры контроллеров классов REST framework
# Аналог контроллера-функции api_rubrics

# class APIRubrics(APIView):
#     def get(self, request):
#         rubrics = Rubric.objects.all()
#         serializer = RubricSerializer(rubrics, many=True)
#         return rest_response(serializer.data)
#
#     def post(self, request):
#         serializer = RubricSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return rest_response(serializer.data, status=status.HTTP_201_CREATED)
#         return rest_response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Более высокоуровневые классы контроллеры

# class APIRubricsv2(ListCreateAPIView):
#     queryset = Rubric.objects.all()
#     serializer_class = RubricSerializer
#
#
# class APIRubricDetail(RetrieveUpdateDestroyAPIView):
#     queryset = Rubric.objects.all()
#     serializer_class = RubricSerializer
