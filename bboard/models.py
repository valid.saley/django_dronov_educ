from django.db import models
from django.contrib.auth.models import User
from django.core import validators
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db.models import ForeignKey


def test_validate(val):
    if val % 2 != 0:
        raise ValidationError('Число нечетное')


# Создание собственного набора записей
# Но для того чтобы пользоватся этим набором записей, нам нужно изменить наш диспетчер записей чуть ниже
# или не менять наш диспетчер, а воспользоваться методами указанными в комментариях после поле bbs в модели Rubric
class RubricQuerySet(models.QuerySet):
    def order_by_bb_count(self):
        return self.annotate(cnt=models.Count('bb')).order_by('-cnt')  # annotate метод именно QuerySet'а


# Это создание собственного менеджера моделей(objects) для модели Rubric
class RubricManager(models.Manager):
    # из атрибута model извлекается используемая модель, из атрибута _db база данных
    def get_queryset(self):
        return RubricQuerySet(self.model, using=self._db)

    # Для того чтобы писать можно было не только так Rubric.bbs.all().order_by_bb_count(), но и так Rubric.bbs.order_by_bb_count() напишем ещё и этот метод
    def order_by_bb_count(self):
        return self.get_queryset().order_by_bb_count()

    # ЭТО БЫЛО ПЕРВОЕ ПРОСТОЕ СОЗДАНИЕ СОБСТВЕННОГО МНЕДЖЕРА МОДЕЛЕЙ
    # def get_queryset(self):
    #     return super().get_queryset().order_by('name')
    #
    # def order_by_bb_count(self):
    #     return super().get_queryset().annotate(cnt=models.Count('bb')).order_by('-cnt')


# Создание диспетчера обратной связи
class BbManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('published')


class Bb(models.Model):
    kinds = (('b', 'buy'), ('s', 'sell'))
    title = models.CharField(max_length=50, verbose_name='Товар')
    content = models.TextField(null=True, blank=True, verbose_name='Описание')
    price = models.FloatField(null=True, blank=True, verbose_name='Цена', validators=[test_validate])
    published = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Опубликовано')
    # как я понял, on_delete определяет поведение при удалении экземпляра связанной модели, тоесть в данном случае при удалении связанной с объявлением рубрики будет ошибка, тк PROTECT не позволяет сделать удаление
    rubric = models.ForeignKey('Rubric', null=True, on_delete=models.PROTECT, verbose_name='Рубрика')
    kind = models.CharField(null=True, max_length=10, choices=kinds)
    objects = models.Manager()
    reverse = BbManager()

    class Meta:
        verbose_name_plural = 'Объявления'  # Имя для множественного числа
        verbose_name = 'Объявление'
        ordering = ['-published']  # сортировка для всех объектов модели

    def clean(self):
        errors = {}
        if not self.content:
            errors['content'] = ValidationError('Укажите описание')
        if self.price and self.price < 0:
            errors['price'] = ValidationError('Укажите неотрицательное значение цены')

        if errors:
            raise ValidationError(errors)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('detail', args=(self.id,))


class Rubric(models.Model):
    name = models.CharField(max_length=20, db_index=True, verbose_name='Название')
    order = models.SmallIntegerField(default=0, db_index=True)
    # Важно что первый объявленный менеджер считается менеджером по умолчанию
    objects = models.Manager()  # дефолтный менеджер
    bbs = RubricManager()  # свой дополнительный менеджер

    # bbs = RubricQuerySet.as_manager() # так или как ниже, пояснение в RubricQuerySet
    # bbs = models.Manager.from_queryset(RubricQuerySet)()

    # objects = RubricManager # можно и так
    # bbs  = models.Manager() # и так
    # теперь Rubric.objects.all() и Rubric.bbs.all() будут разные реузльтаты

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Рубрики'
        verbose_name = 'Рубрика'
        ordering = ['order', 'name']


class AdvUser(models.Model):
    is_activated = models.BooleanField(default=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)


class Spare(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Machine(models.Model):
    name = models.CharField(max_length=30)
    spares = models.ManyToManyField('Spare', through='Kit', through_fields=(
        'machine_id', 'spare_id'))  # формат through_fields - ведущая модель, ведомая модель
    notes = GenericRelation('Notes')  # Поле для связи с полиморфной моделью

    def __str__(self):
        return self.name


# Эту таблицу мы создаем чтобы добавить в связующую таблицу ManyToMany дополнительные поля, в нашем случае count
# Чтобы замигрейтить эту таблицу прищлось удалять таблицы машин и деталей и создать заного,
# потому что нужно написать кастомные миграции для таких вещей
class Kit(models.Model):
    machine_id = models.ForeignKey('Machine', on_delete=models.CASCADE)
    spare_id = models.ForeignKey('Spare', on_delete=models.CASCADE)
    count = models.IntegerField()


# Таблица для полиморфной связи, лучше просто прочитать ещё раз в книге
class Notes(models.Model):
    content = models.TextField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey(ct_field='content_type', fk_field='object_id')


class Message(models.Model):
    message = models.TextField()


class PrivateMessage(Message):
    user = ForeignKey(User, on_delete=models.CASCADE)


