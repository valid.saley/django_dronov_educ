from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import escape  # заменяет недопустимые символы
from django.utils.safestring import mark_safe  # Помечает строку как ту, в которой произошли замены
from django.utils.safestring import SafeText  # Помеченные строки представляются как экземпляры этого класса
from django.utils.html import format_html

register = template.Library()


@register.filter(name='cur',
                 is_safe=True)  # регистрация фильтра через декоратор, is_safe значит заменять недопустимые символы, ещё можно этот функционал расписать более подробно и сложно, это глава 18.3.2.2
# @stringfilter  # преобразование нестрокового параметра в строковый
def currency(value, name='руб.'):  # использовали в detail
    string = '%1.2f %s' % (value, name)
    return string


# register.filter('currency', currency)  # Регистрация своего фильтра


@register.simple_tag
def lst(sep, *args):
    # Получается что mark_safe не просто помечает строку как безопасную, но и выводит строку в формате html кода, также можно юзать format_html
    return mark_safe(f'{sep.join(args)} (итого<strong>{len(args)}</strong>)')


@register.inclusion_tag('tags/ulist.html')
def ulist(*args):
    return {'items': args}
