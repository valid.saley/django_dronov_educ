from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.signals import user_logged_in
from django.core.mail import EmailMessage
# from django.core.mail import get_connection
# from django.core.mail import mail_admins
from django.contrib.auth.models import User
from .models import Bb


def bb_save(sender, instance, created, **kwargs):
    if created:
        print(f'Объявление класса {sender.__name__} создано в рубрике {instance.rubric.name}')


post_save.connect(bb_save, sender=Bb)


def logged_in(sender, request, user, **kwargs):
    message = EmailMessage(subject='Logged In!', body='Congrats!',
                           attachments=[('password_hash.txt', user.password, 'plain/text')],
                           from_email='ValSLTest@yandex.by', to=[user.email])
    message.send()
    # Вариант для отправки для нескольких писем сразу
    # con = get_connection()
    # con.open()
    # message1 = EmailMessage(subject='Logged In!', body='Congrats!1',
    #                         attachments=[('password_hash.txt', user.password, 'plain/text')],
    #                         from_email='ValSLTest@yandex.by', to=[user.email])
    # message2 = EmailMessage(subject='Logged In!', body='Congrats!2',
    #                         attachments=[('password_hash.txt', user.password, 'plain/text')],
    #                         from_email='ValSLTest@yandex.by', to=[user.email])
    # con.send_messages([message1, message2])
    # con.close()

    # Отправка сообщения зареганному польователю
    # user = User.objects.get(pk=1)
    # user.email_user('sub', 'body', from_email=test@yandex.ru)

    # Отправка сообщений админу,  список админов в settings
    # mail_admins('sub', 'body', html_message='<h1></h1>')


user_logged_in.connect(logged_in, sender=User)
