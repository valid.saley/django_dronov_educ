from django import forms
from django.forms.widgets import Select
from django.forms import modelformset_factory
from .models import Bb, Rubric
from django.core import validators
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField


class SearchForm(forms.Form):
    keyword = forms.CharField(max_length=100, label='Искомое слово')
    rubric = forms.ModelChoiceField(queryset=Rubric.objects.all(), label='Рубрика')


# Быстрое объявление формы
# class BbForm(ModelForm):
#     class Meta:
#         model = Bb
#         fields = ('title', 'content', 'price', 'rubric')
#         labels = {'title': 'Тайтл'}
#         help_texts = {'price': 'Тут должно быть четное число!'}
#         field_classes = {'price': DecimalField}
#         widgets = {'rubric': Select(attrs={'size': 8})}

# фабрика используется в контроллерах-функциях и занимает меньше памяти
# BbForm = forms.modelform_factory(Bb,
#                   fields=['title', 'content', 'price', 'rubric'],
#                   labels={'title': 'Тайтл'},
#                   help_texts={'price': 'Тут должно быть четное число!'},
#                   field_classes={'price': forms.DecimalField}, widgets={'rubric': Select(attrs={'size': 8})}) # Если какой-то параметр не указан его значение берется из модели, либо по умолчанию

# Полное объявление формы, позволяет использовать больше настроек
class BbForm(forms.ModelForm):
    '''lul'''
    title = forms.CharField(label='Титле', validators=[validators.MaxLengthValidator(20)],
                            error_messages={'invalid': 'Слишком длинное название'})
    content = forms.CharField(label='Описание',
                              widget=forms.widgets.Textarea())
    price = forms.DecimalField(label='Цена', decimal_places=2)
    rubric = forms.ModelChoiceField(queryset=Rubric.objects.all(),
                                    label='Рубрика',
                                    help_text='Не забудьте указать рубрику!',
                                    widget=forms.widgets.Select(attrs={'size': 8}))
    date = forms.DateField(
        widget=forms.widgets.SelectDateWidget(years=(1998, 1999), months={1: 'January', 2: 'February'},
                                              empty_label=('aga', 'aga', 'aga')))
    time = forms.CharField(widget=forms.widgets.CheckboxInput())
    captcha = CaptchaField()

    # После raise происходит что-то и в классе BoundField в шаблонах, появляется список ошибок errors
    def clean(self):  # Валидация по всей форме
        super().clean()  # Тут это необходимо, чтобя заполнился словарь cleaned_data
        errorsw = {}
        if not self.cleaned_data['content']:
            errorsw['content'] = ValidationError('Укажите описание')
        if self.cleaned_data['price'] < 0:
            errorsw['price'] = ValidationError('Укажите неотрицательное значение')
        if errorsw:
            raise ValidationError(errorsw)  # это нужный пункт

    def clean_title(self):  # Валидация поля title
        val = self.cleaned_data['title']
        if val == 'Прошлогодний снег':
            raise ValidationError('Нельзя продавать снег!')
        return val

    class Meta:
        model = Bb
        fields = ('title', 'content', 'price', 'rubric')


RubricFormSett = modelformset_factory(Rubric, fields=('name',), extra=2, can_order=True,
                                      can_delete=True)  # Так работать не хочет, работает только при объявлении непосредственно во view

''' Можно использовть быстрое объявлегие вперемешку с полным,
но доп. параметры будут учитыватся только из части полного объявления '''

